#!/bin/bash -l
# The -l above is required to get the full environment with modules

# Set the allocation to be charged for this job
# not required if you have set a default allocation
#SBATCH -A 2017-21

# The name of the script is myjob
#SBATCH -J AllScale

# Only 1 hour wall-clock time will be given to this job
#SBATCH -t 05:00:00

# Number of nodes
#SBATCH -N 1

# Nodes with 4 NUMA domains
#SBATCH --mem=1000000

#SBATCH -e error_ipic3d0407.o
#SBATCH -o output_ipic3d0407.o

module load gcc/4.9.2
module load openmpi/1.8-gcc-4.9 

export INPUT1=../inputfiles/tiny_1p.inp
export INPUT2=../inputfiles/tiny_2p.inp
export INPUT4=../inputfiles/tiny_4p.inp
export INPUT8=../inputfiles/tiny_8p.inp

# Run the executable named myexe 
mpiexec -n 8 ./../build/iPIC3D $INPUT8
mpiexec -n 4 ./../build/iPIC3D $INPUT4
mpiexec -n 2 ./../build/iPIC3D $INPUT2
mpiexec -n 1 ./../build/iPIC3D $INPUT1
